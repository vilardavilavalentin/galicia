FROM valenvilardav/galicia-hello-world:nginx

EXPOSE 80 443

COPY index.html /usr/share/nginx/html/index.html

CMD ["nginx", "-g", "daemon off;"]
