# Evaluacion Galicia

### Consigna

Se solicita la creacion de una pipeline para el despliegue de una pagina web que contenga una imagen y la frase:
"Esta es una aplicación realizada para examen de selección de Banco Galicia"

Las herramientas, lenguaje y plataforma donde se realice el ejercicio queda a criterio del candidato.

### Resolucion

Para desplegar la pagina web se creo un cluster de kubernetes utilizando GKE autopilot.

Se creo un repo en gitlab para almacenar el codigo fuente.

Para generar la compilacion y hacer el despliegue del codigo fuente se utiliza la herramienta gitlab-ci.

La pipeline cuenta con dos stages, uno de build y otro de deploy.
En el primer stage se buildea la imagen de Docker utilizando kaniko, esta imagen consta de un nginx al que se le agrega un archivo index.html que es la pagina web, esta imagen luego se sube al registry de gitlab.
Cabe aclarar que la imagen de nginx que se utiliza es una version personalizada que ya tiene modificada la configuracion de Nginx para que rediriga el trafico hacia el index.html.
Luego se deploya en el cluster de GKE bajo la url: https://k8s.valenvilardav.xyz/galicia 
El stage de deploy consta de la aplicacion de un deployment.
Para poder conectarse al cluster desde la pipeline se creo una service account con permisos solo sobre el namespace galicia.
Previamente en el cluster se creo un service, un ingress, un secreto para poder conectarse al registry de gitlab y un secreto con el certificado TLS.
La pipeline se triggerea al modificar el archivo index.html, por lo que si se realiza un cambio se puede observar la nueva pagina deployada en: https://k8s.valenvilardav.xyz/galicia 


